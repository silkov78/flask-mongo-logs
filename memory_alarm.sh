
api_url="http://127.0.0.1:8080/logs/"

while true
do
	mem_usage=$(free | awk '/Mem/{print $3/$2*100}')
	datetime=$(date +"%Y-%m-%d %H:%M:%S")

	echo '{"datetime": "'"$datetime"'", "MEM_usage, %": '"$mem_usage"'}'

	curl -X POST $api_url \
	-H "Content-Type: application/json" \
	-d '{"datetime": "'"$datetime"'", "MEM_usage, %": '"$mem_usage"'}'


	echo
	sleep 5
done

