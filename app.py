from flask import Flask, request, jsonify
import pymongo
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient(host='mongodb_host',
                     port=27017,
                     username='root',
                     password='pass',
                     authSource="admin")

db = client['db_logs']
collection = db['collection_logs']


@app.route('/logs/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        data = request.get_json()
        collection.insert_one(data)
        return 'Data added to MongoDB successfully'
    else:
        cursor = collection.find({})
        all_logs = [{
            "datetime": log["datetime"],
            "MEM_usage, %": log["MEM_usage, %"]}
            for log in cursor]

        return {"all_logs": all_logs}


@app.get('/logs/<int:row_number>/')
def certain_log_get(row_number: int):
    if row_number < 1:
        return "Row nummber should be more the 1!"

    record_cursor = collection.find().skip(row_number - 1).limit(1)

    if not record_cursor:
        return f"Object with row number {row_number} is not found!"

    record = [{
        "datetime": log["datetime"],
        "MEM_usage, %": log["MEM_usage, %"]}
        for log in record_cursor]

    return record


@app.put('/logs/<int:row_number>/')
def certain_log_put(row_number: int):
    if row_number < 1:
        return "Row nummber should be more the 1!"

    record_cursor = collection.find().skip(row_number - 1).limit(1)[0]

    if not record_cursor:
        return f"Object with row number {row_number} is not found!"

    result = collection.update_one(
        {"_id": record_cursor["_id"]},
        {"$set": dict(request.json)}
    )

    if not result.modified_count:
        return {
            "message": "No changes applied"
        }

    return {"message": "Update success"}, 200


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080)
